# README #

### To test out the login mechanism
* username: dummy
* password: lolk

### Install Instructions ###
Download node.js from official site

In powershell or bash from source folder:
    npm install ### Installs dependencies
Linux
    DEBUG=myapp:*
Powershell
    set DEBUG=myapp:*

npm start

The application is now running on localhost ports
http://127.0.0.1:80
https://127.0.0.1:443