var express = require('express');
var router = express.Router();
var backend = require('../backend');
var ObjectId = require("mongodb").ObjectId;
/* GET home page. */
router.get('/', function(req, res) {
  //res.render('calendar', { title: `${req.session.uname}` });
  if (req.query.id || 0) {
    console.log(req.query);
    /*
    backend.search_patient_id(ObjectId(req.query.id)).then(function(result) {
      res.send(result);
    });
    */
    backend.search_name_patient(req.query.id).then(function(result) {
      res.send(result);
    });
  } else {
    res.render('record', {title: 'Records'});
  }
});

router.get('/addRecord', function(req, res) {
  if (req.query.pname) {
    res.render('newrecord', {pname: req.query.pname, rnumber: req.query.rnumber});
  } else {
    res.render('error', {status: 'No patient name/id detected'});
  }
});

router.post('/addRecord', function(req, res) {
  backend.add_record_patient(ObjectId(req.body.id), req.body.rid, JSON.parse(req.body.details)).then(function(result) {
    console.log(result);
  });
});

module.exports = router;
