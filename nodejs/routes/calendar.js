var express = require('express');
var router = express.Router();
const backend = require('../backend');
const { body, validationResult } = require('express-validator');
const ObjectId = require("mongodb").ObjectId;/* GET home page. */

router.get('/', function(req, res, next) {
  res.render('calendar', {title: 'Calendar'});
});

router.post('/aptadd', function(req,res) {
  var year = req.body.year;
  var month = req.body.month;
  var day = req.body.day;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var details = " Name: " +  firstName + " " + lastName + "     Type: " + req.body.details;
  var duration = req.body.duration;
  var hour = req.body.hour;
  var minute = req.body.minute;

  console.log("Added appointment");
  backend.push_schedule(req.session.uname,
    Number(year),
    Number(month),
    Number(day),
    Number(hour),
    Number(minute),
    Number(duration),
    details).then(function(result) {
      console.log(result);
      res.json(result);
      res.status(200);
    });
});

router.get('/getapt', function(req, res) {
  //backend.search_detail_schedule(req.session.uname, '');
  var year = Number(req.query.year);
  var month = Number(req.query.month);
  var day = Number(req.query.day);

  backend.fetch_date_schedule(req.session.uname,year,month,day).then( function(result) {
    res.json(result);
    res.status(200);
  });
});

router.post('/delapt',function(req,res){
  var id = req.body.id;

  backend.delete_schedule(ObjectId(id)).then(function(result){
    console.log(result);
    res.send(result);
    res.sendStatus(204);
  });
});

module.exports = router;
