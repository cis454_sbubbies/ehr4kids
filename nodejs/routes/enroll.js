var express = require('express');
var router = express.Router();
var backend = require("../backend");
/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('calendar', { title: `${req.session.uname}` });

  res.render('enroll', {title: 'Enroll'});

});

router.post('/', function(req, res, next) {
  console.log(req.body);
  backend.push_patient(JSON.parse(req.body.str));
  res.redirect("/enroll");
});
module.exports = router;
