const express = require('express');
const router = express.Router();
const backend = require('../backend');
const { body, validationResult } = require('express-validator');


router.get('/', function(req, res, next) {
  res.render('login', {title: 'Login'});
});

router.post('/', 
  // Input validation and sanitization
  [
    body('uname')
      .not().isEmpty()
      .trim()
      .escape(),
    body('pword')
      .not().isEmpty()
  ], 

  function(req, res, next) {
    var uname = req.body.uname;
    var pword = req.body.pword;

    const errors = validationResult(req);

    backend.login(req.body.uname, req.body.pword).then(function(success) {
      if (success) {
        req.session.uname = uname;
        res.redirect('/calendar');
      }
      else {
        res.render('login', { title: "Login", error: "Login failed"});
      }
    });
  }
);

module.exports = router;
