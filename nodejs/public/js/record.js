//get record and display to screen
$(document).ready(function() {
  //Display record function
  var nameIndex = {};
  var allPatientObjReturned;
  $(document).on("click",".record",function(){
    // the '+ 1' gives the element id of the record panel in the html document
    var contentPanelId = $(this).attr("id");
    try{
      var recordId = String(localStorage.getItem('lastRecordViewed')+'1');
      if(recordId.length!=0){
        console.log('Hurray');
        console.log('Last Record Was: '+recordId);
        var lastElement = document.getElementById(recordId);
        lastElement.style.backgroundColor = "white";
      }
    }catch(error){
      console.log('Boo');
    }

    $(this).css("backgroundColor","#FFBF00");

    $(document).ready(function(){
          // change color to active color
          var patient = JSON.parse(localStorage.getItem('patientData'));
          var currentPatientRecord = (patient.records[String($("input[name='recordWidget']:checked").val())]);
          console.log(currentPatientRecord.recordTitle);

          $("#mainRightBodyRight").html('<div class="displayFrame"><h2>'+currentPatientRecord.recordTitle+'</h2><br><h4>Patient Name: '+patient.biography.patientName+'</h4><h4>Patient ID: '+patient._id+'</h4><h4>Hospital: '+currentPatientRecord.hospital+'</h4><h4>Doctor: '+currentPatientRecord.doctor+'</h4><h4>Date: '+currentPatientRecord.date+'  Time:'+currentPatientRecord.time+'</h4><br><p>'+currentPatientRecord.recordContent+'</p></div>');

          localStorage.setItem('lastRecordViewed',$("input[name='recordWidget']:checked").val());
        });
  });


  //generate the list of records on the left hand
  //also generates pateint record mgr button
  function generateRecordList(){
    var patient = JSON.parse(localStorage.getItem('patientData'));
    // if list is empty or is a different patient
    $('#recordList').empty();

    $('.mainRightBody').html('<div class="mainRightBodyLeft"><form id = "recordList"><!--Records will be generated and put here by JSON--></form></div><div id="mainRightBodyRight"></div>');
    // try to learn to link back to css document
    $('.mainRightBody').css({"width":"100%","height":"calc(100% - 14.4em)","display":"grid","grid-template-columns":"430px auto"});

    $.each(patient.records, function(key, value){
    console.log(key+': '+this.hospital);
    $('#recordList').append('<input type="radio" name="recordWidget" id="'+key+'" value="'+key+'" hidden><label for = "'+key+'" class="recordLabel"><div class = "record" id="'+key+'1"> <div class="recordContainer"><div class="recordInfo"> <h4>'+ patient.biography.patientName+'</h4> <h6>'+this.hospital+'</h6><p>'+this.doctor+'<br>'+this.date+':'+this.time+'</p></div><div class="recordType"><h4>'+this.recordType+'</h4></div></div></div></label>');
    });
  //Generate Patient Button in right corner
    $(".mainRightTopToolBarBoxRightContainer").html('<input type="checkbox" id="patientWindowCheck" hidden><label for="patientWindowCheck" class="patientInfoBoxContainer"><div class="patientInfoBox"><h4>'+
    patient.biography.patientName+'</h4><h6>Address: '+patient.biography.address+'</h6><p>Gender: '+patient.biography.sex +'<br>Dob: '+patient.biography.DOB +'</p></div></label>');

  }

  $("#idSearchButton").click(function(){

    get('/records',"id="+String($('#idSearch').val()), function(result) {
      $('#nameSearch').show();
      allPatientObjReturned = JSON.parse(result);
      $('#nameReturned').html('');
      $(allPatientObjReturned).each(function(index){
        nameIndex[this.biography.patientName] = index;
        $('#nameReturned').append('<li class="nameReturnedEl" id="'+index+'">'+this.biography.patientName+'</li>');
      });
      console.log(nameIndex);
    });
  });

  $(document).on("click",".nameReturnedEl",function(){
    $('#nameSearch').hide();
    $('#idSearch').val();
    localStorage.setItem('patientData',JSON.stringify(allPatientObjReturned[$(this).attr('id')]));
    generateRecordList();
  });

  // if enter pushed on search Quality of Life
  var input = document.getElementById("idSearch");
  input.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
     event.preventDefault();
     document.getElementById("idSearchButton").click();
    }
  });

  //filter checkboxes function
  $('.filterCheckBox').bind("change",function(){
    /* this functions will scale poorly in future it should be optimized currently running at O(NxM) but it is fine for testing purposes*/
    var listOfChecked = [];
    $('.filtersContainer').each(function(){
      $(this).find('.checkBoxContainer').each(function(){
        $(this).find('.filterCheckBox').each(function(){
          if($(this).prop("checked")){
            listOfChecked.push($(this).val());
          }});});});
    console.log(listOfChecked);

    if(listOfChecked.length==0){
      $('#recordList').each(function(){
        $(this).find('label').each(function(){
          $(this).show();
        });
      });
    }else{
      $('#recordList').each(function(){
        $(this).find('label').each(function(){
          if(!(listOfChecked.includes(($(this).find(".recordType").text())))){
            $(this).hide();

          }else{
            $(this).show();
          }
        });
      });
    }
  });


  // reload body dive to mannage records page
  $(document).on("click","#patientWindowCheck",function(){
    var patient = JSON.parse(localStorage.getItem('patientData'));
    if($('#patientWindowCheck').prop("checked")) {
       localStorage.setItem('mainRightBodyLastState',$(".mainRightBody").html());
       //'+ patient.biography.patientName+'
       $('.mainRightBody').html(
  '<div class="body">' +
    '<div class="container">'+
      '<header id="patientName">'+
        '<div class="contentPadding">'+
        '<h2>'+ patient.biography.patientName+'</h2>'  +
      '</div>' +
    '</header>'  +

    '<div class="sectionTitle">'  +
      ' <div class="sectionTitlePadding">' +
      '<h5>General Information</h5>'     +
      '</div>'   +
    '</div>'   +

    '<div class="mainContentPadding">'   +
      '<div id="generalInfoContent">'   +
        '<div id="generalPersonal">'   +
          '<div class="contentPadding">'   +
            '<h5>Personal Information:</h5>'   +
            '<p><b>Address:</b> '+ patient.biography.address+
            ' <br><b>DOB:</b> '+ patient.biography.DOB+
            '<br><b>Sex:</b> '+ patient.biography.sex+
            '<br><b>Class:</b> '+ patient.biography.class+
            '<br><b>Staying with:</b> '+ patient.biography.stayWName+
            '<br><b>contact:</b> '+ patient.biography.stayWContact+
          '</div>'   +
        '</div>'   +

        '<div id="generalParents">'   +
          '<div class="contentPadding">'   +
          '<h5>Parent Information:</h5>'     +
          '<p><b>Father\'s Name:</b> '+ patient.parents.father.name+
          '<br><b>occupation:</b> '+ patient.parents.father.occupation+
          '<br><b>Mother\'s Name:</b>'+ patient.parents.mother.name+
          '<br><b>occupation:</b> '+ patient.parents.mother.occupation+
          '</div>'   +
        '</div>'   +

        '<div id="generalBiometrics">'   +
          '<div class="contentPadding">'  +
            '<h5>Biometrics:</h5>'   +
            '<p><b>Height (in cms):</b>  '+ patient.biometrics.height+
            '<br><b>Waist Circumference (in cms):</b>  '+ patient.biometrics.waist+
            '<br><b>Chest (Normal) in cms:</b>  '+ patient.biometrics.chest+
            '<br><b>Pulse:</b> '+ patient.biography.pulse+
            '<br><b>Respiratory rate:</b> '+ patient.biometrics.respiratory+
            '<br><b>BMI:</b> '+ patient.biography.BMI+
          '</div>'   +
          '<div id="rightBiometrics">'   +
            '<p><b>Weight (in kgs):</b>  '+ patient.biometrics.weight+
            '<br><b>Head Circumference (in cms):</b>  '+ patient.biometrics.headCirc+
            '<br><b>Temperature:</b> '+ patient.biography.temperature+
            '<br><b>BP:</b> '+ patient.biometrics.BP+
            '<br><b>Last updated:</b> '+ patient.biometrics.lastUpdated+' <b>by:</b> '+ patient.biometrics.docName+
          '</div>'   +
        '</div>'   +
      '</div>'   +
    '</div>'   +

    '<div class="sectionTitle">'   +
      '<div class="sectionTitlePadding">'   +
        '<h5>Examinations</h5>'   +
      '</div>'   +
    '</div>'   +

    '<div class="mainContentPadding">'   +
      '<div class="contentPadding">'   +
        '<table id="examinationsContent">'   +
          '<tr>'   +
            '<td><b>Physical Examinations</b></td>'   +
            '<td class="incomplete">incomplete</td>'   +
            '<td><a href="#">view</a></td> '   +
            '<td><a href="#">edit</a></td>'   +
          '</tr> '  +
          '<tr>'   +
          '<td><b>Cardio Vascular System</b></td>'     +
            '<td class="complete">Complete</td>'     +
            '<td><a href="#">view</a></td>'   +
            '<td><a href="#">edit</a></td>'   +
          '</tr>'   +
          '<tr>'   +
            '<td><b>Respiratory System</b></td>'   +
            '<td class="incomplete">incomplete</td>'   +
            '<td><a href="#">view</a></td>'   +
            '<td><a href="#">edit</a></td>'   +
          '</tr>'   +
        '</table>'   +
      '</div>'   +
    '</div>'   +

    '<div class="sectionTitle">'   +
      '<div class="sectionTitlePadding">'   +
        '<h5>Self Assesment</h5>'   +
      '</div>'   +
    ' </div>'  +

    '<div class="mainContentPadding">'   +
      '<div class="contentPadding">'   +
        '<table id="examinationsContent">'   +
          '<tr>'   +
            ' <td><b>Micro Students Self Adminis</b></td>'  +
            '<td class="incomplete">incomplete</td>'   +
            '<td><a href="#">view</a></td>'   +
            '<td><a href="#">edit</a></td> '  +
          '</tr>'   +
          '<tr>'   +
          '<td><b>Mental Health Questionnaire</b></td>'     +
          '<td class="complete">Complete</td>'     +
          '<td><a href="#">view</a></td>'     +
          '<td><a href="#">edit</a></td>'     +
          '</tr>'   +
        '<tr>'     +
          '<td><b>Self Esteem Asses</b></td>'     +
          '<td class="incomplete">incomplete</td>'     +
          '<td><a href="#">view</a></td>'     +
          '<td><a href="#">edit</a></td>'     +
          '</tr>'   +
        '</table>'   +
      '</div>'   +
    '</div>'   +

    '<div class="sectionTitle">'   +
      '<div class="sectionTitlePadding">'   +
        '<h5>Record Summary</h5>'   +
      '</div>'   +
    '</div>'   +

    '<div class="mainContentPadding">'   +
      '<div id="recordSummaryContent">'   +
        '<div class="contentPadding">'   +
          '<h5>Record Count:</h5>'   +
          '<p><b>HR:</b>  167cm'   +
          '<br><b>DN:</b>  Agent'   +
          '<br><b>LBR:</b>  Helen Smith'   +
          '<br><b>GH:</b> Agent</p>'   +
          '<button type="button" id="addRecordButton">Add new Record</button>'   +
          '<button type="button" id="requestEditButton">Request Edit</button>'   +
        '</div>'   +
      '</div>'   +
    '</div>'   +
  '</div>'   +
  '</div>' );
       $('.mainRightBody').css("display","block");
     } else {
       $(".mainRightBody").html(localStorage.getItem('mainRightBodyLastState'));
       $(".mainRightBody").css({"display":"grid","grid-template-columns":"430px auto"}) ;
     }
  });

  $(document).on("click","#addRecordButton",function(){
    var patient = JSON.parse(localStorage.getItem('patientData'));

    window.open('/records/addRecord?pname='+patient.biography.patientName+'&rnumber='+patient._id, "get");
  });

});
