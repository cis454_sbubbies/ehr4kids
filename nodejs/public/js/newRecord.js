var inputs = $('.recordInputActive');
var form = {};
var patient = JSON.parse(localStorage.getItem('patientData'));
var recordId = Math.round(Math.random() * 10);
var popUp = $('#previewPopup');

function stageRecord(){
	inputs.each(function(){
		$(this).attr('disabled',true);
		$(this).removeClass('recordInputActive');
		$(this).addClass('recordInputStaged');
	});
	$("#addRecordButton").attr('hidden',true);
	$("#eidtRecordButton").attr('hidden',false);

	$("#confirmRecordButton").attr('hidden',false);
	$('#previewPopup').show();

}

function closePopUp(){
	$('#previewPopup').hide();
}

function unStageRecord(){
	inputs.each(function(){
		$(this).attr('disabled',false);
		$(this).removeClass('recordInputStaged');
		$(this).addClass('recordInputActive');
	});
	$("#addRecordButton").attr('hidden',false);
	$("#eidtRecordButton").attr('hidden',true);
	$("#confirmRecordButton").attr('hidden',true);
}

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

function commitRecord(){
	var patient = JSON.parse(localStorage.getItem('patientData'));
	inputs.each(function(){
		form[String($(this).attr('id'))]=$(this).val();
	});

	var recID = randomString(12, '0123456789abcdefghijklmnopqrstuvwxyz');
	if(!(patient.records[String(recordId+'-'+patient._id)])){

		patient.records[recID]=form;
		console.log(patient);
		$('#formContainer').attr('hidden',true);
		$('#pushSucessful').css('display','grid');
		submit('/records/addRecord', {id: patient._id, rid: recID, details: JSON.stringify(form) });
	}


}
