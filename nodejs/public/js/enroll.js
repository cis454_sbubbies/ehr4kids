$(document).ready(function(){
	var forms = $('#welcome');
	$('.mainRightBodyContentPadding').find('.enrollmentForm').each(function(){
		forms.push($(this));
	});
	forms.push($('#confirmEnrollment'));
	var percent = 0;
	var state=0;

	percent = (1/forms.length)*100;

	console.log("forms: "+String(forms.length));
	$('#progressBar').css('width',String(percent+'%'));

	$('.newRecordButton').bind("click",function(){
		var goBackIds = ["previousButton","cancelButton"];
		var goForwardIds = ["enrollButton","nextButton"];
		if(goBackIds.includes($(this).attr("id"))){
			$(forms).each(function(){
				$(this).hide();
			});
			state--;
			$(forms[state]).show();
			percent = ((state+1)/forms.length)*100;
			$('#progressBar').css('width',String(percent+'%'));
		}else if(goForwardIds.includes($(this).attr("id"))){
			$(forms).each(function(){
				$(this).hide();
			});
			state++;
			$(forms[state]).show();
			percent = ((state+1)/forms.length)*100;
			$('#progressBar').css('width',String(percent+'%'));
		}
		if(state!=0&&state!=(forms.length-1)){$('#formNavButtons').show()}
		else{$('#formNavButtons').hide()}

		if(state==(forms.length-1)){
			$(forms).each(function(){
				$(this).show();
				$(this).children().prop( "disabled", true );
			});
			$(forms[0]).hide();
			$('#confirmEnrollment').children().prop( "disabled", false );
			$("#pleaseReviewform").show();

		}else{
			$("#pleaseReviewform").hide();
			$(forms).each(function(){
				$(this).children().prop( "disabled", false );
			});
		}
	});

	console.log(forms.length);
	forms.each(function(){
		console.log($(this).attr('id'));
	});

 $('#confirmSubmitButton').on("click",function(){
	 var patientObject =
	 '{' +
		 '"biography": { '+
			 '"patientName": "'+ $('#studentName').val() +'",' +
			 '"class": "'+$('#classYear').val()+'",' +
			 '"address": "'+$('#address').val()+'",' +
			 '"sex": "'+ $('#sex').val()+'",' +
			 '"DOB": "'+$('#DOB').val() +'" '+
		 '},' +
		 '"parents": {' +
			 '"father": {' +
				 '"name": "'+ $('#fatherName').val()+'",' +
				 '"occupation": "'+ $('#fatherOccupation').val()+'"' +
			 '},' +
			 '"mother": {' +
				 '"name": "'+$('#motherName').val() +'",' +
				 '"occupation": "'+$('#motherOccupation').val() +'"' +
			 '}' +
		 '},' +
		 '"biometrics": {' +
		    '"height": "'+ $('#height').val()+'",' +
		 		'"waist" :"' + $('#waist').val()+'",' +
		 		'"chest" :"' + $('#chest').val()+'",' +
		 		'"pulse" :"' + $('#pulse').val()+'",' +
		 		'"respiratory" :"'+ $('#respiratory').val()+'",' +
		 		'"BMI":"' + $('#BMI').val()+'",' +
		 		'"weight":"' + $('#weight').val()+'",' +
		 		'"headCirc":"' + $('#headCirc').val()+'",' +
				'"BP":"' + $('#BP').val()+'",' +
		 		'"temperature" :"' + $('#temperature').val()+'",' +
		 		'"lastUpdated" :"' + $('#lastUpdated').val()+'",' +
				'"docName" :"' + $('#doctorName').val() +
		 '"},'  +
		 '"examinations":{},' +
		 '"selfAssesment":{},'+
		 '"records": {}}';
	 //alert(JSON.stringify(JSON.parse(patientObject)));
	 //router.post("/records", JSON.parse(patientObject), method = 'post');

	 submit('/enroll', { str: patientObject });
 });
});

function setHeight() {
		$(".mainRightBodyContainer").height($(".mainRight").height() - ($(".mainRightTopHeader").height()*2));
}
$(document).ready(setHeight);
$(window).resize(setHeight);
