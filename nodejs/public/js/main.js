
today = new Date();
nowMonth = today.getMonth();       //get current month
currentMonth = today.getMonth();   //save current month
nowYear = today.getFullYear();      //get current year
currentYear = today.getFullYear();   //save current year
currentDate = 0;
var meetList = [];   //meetList holds list of keys
var toDatabase = []; //list that holds all appointment objects
var currentApt = 0;  //initalize current Apt for later
var selectedCell;  //stores the selected date in table

//initial function to set up calendar
clearCalendar();
createCalendar();

function get(url, params, callback) {
    console.log("get async");
    console.log(params);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200)
            callback(xhr.responseText);
    };
    xhr.open('GET', url+"?"+params, true);
    xhr.send(null);
}

function post(url, params, callback) {
  console.log("get async");
  console.log(params);
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200)
          callback(xhr.responseText);
  };
  xhr.open('POST', url, true);
  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhr.send(JSON.stringify(params));
}
/**
* sends a request to the specified url from a form. this will change the window location.
* @param {string} path the path to send the post request to
* @param {object} params the paramiters to add to the url
* @param {string} [method=post] the method to use on the form
*/
function submit(path, params, method = 'post') {
    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    console.log('post');
    const form = document.createElement('form');
    form.method = method;
    form.action = path;
    for (const key in params) {
        if (params.hasOwnProperty(key)) {
            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = key;
            hiddenField.value = params[key];
            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}

//clearCalendar() clears the old calendar table
function clearCalendar() {
    console.log('clear');
    for (var rowCount = 1; rowCount <= 6; ++rowCount) {
        for (var colCount = 0; colCount <= 6; ++colCount) {
            table.rows[rowCount].cells[colCount].innerHTML = " ";
        }
    }
}

//createCalendar() fills in calendar table using Date type
function createCalendar() {
    console.log('create');
    startYear = currentYear;
    startMonth = currentMonth;
    firstDay = new Date();
    firstDay.setFullYear(startYear, startMonth, 1);
    numCount = choseDateCount(startMonth, firstDay);
    colCount = firstDay.getDay();
    rowCount = 1;
    for (var date = 1; date <= numCount; ++date) {
        table.rows[rowCount].cells[colCount].innerHTML = date;
        colCount = colCount + 1;
        if (colCount == 7) {
            rowCount = rowCount + 1;
            colCount = 0;
        }
    }
}

//choseDateCount() determins how many dates are in the month
function choseDateCount(startMonthTwo, firstDayTwo) {
    console.log('chose');
    var numDates = 0; //return value
    var startYearTwo = firstDayTwo.getFullYear();
    document.getElementById("demo2").innerHTML = startYearTwo; //display year to page
    //find the current month when changing calendar table
    //by hitting next or prev buttons
    if (startMonthTwo < 0) {
        if (startMonthTwo % 12 == 0) {
            startMonthTwo = 0;
        }
        else if (Math.abs(startMonthTwo) % 12 == 1) {
            startMonthTwo = 11;
        }
        else if (Math.abs(startMonthTwo) % 12 == 2) {
            startMonthTwo = 10;
        }
        else if (Math.abs(startMonthTwo) % 12 == 3) {
            startMonthTwo = 9;
        }
        else if (Math.abs(startMonthTwo) % 12 == 4) {
            startMonthTwo = 8;
        }
        else if (Math.abs(startMonthTwo) % 12 == 5) {
            startMonthTwo = 7;
        }
        else if (Math.abs(startMonthTwo) % 12 == 6) {
            startMonthTwo = 6;
        }
        else if (Math.abs(startMonthTwo) % 12 == 7) {
            startMonthTwo = 5;
        }
        else if (Math.abs(startMonthTwo) % 12 == 8) {
            startMonthTwo = 4;
        }
        else if (Math.abs(startMonthTwo) % 12 == 9) {
            startMonthTwo = 3;
        }
        else if (Math.abs(startMonthTwo) % 12 == 10) {
            startMonthTwo = 2;
        }
        else if (Math.abs(startMonthTwo) % 12 == 11) {
            startMonthTwo = 1;
        }
    }
    else {
        if (startMonthTwo % 12 == 0) {
            startMonthTwo = 0;
        }
        else if (startMonthTwo % 12 == 1) {
            startMonthTwo = 1;
        }
        else if (startMonthTwo % 12 == 2) {
            startMonthTwo = 2;
        }
        else if (startMonthTwo % 12 == 3) {
            startMonthTwo = 3;
        }
        else if (startMonthTwo % 12 == 4) {
            startMonthTwo = 4;
        }
        else if (startMonthTwo % 12 == 5) {
            startMonthTwo = 5;
        }
        else if (startMonthTwo % 12 == 6) {
            startMonthTwo = 6;
        }
        else if (startMonthTwo % 12 == 7) {
            startMonthTwo = 7;
        }
        else if (startMonthTwo % 12 == 8) {
            startMonthTwo = 8;
        }
        else if (startMonthTwo % 12 == 9) {
            startMonthTwo = 9;
        }
        else if (startMonthTwo % 12 == 10) {
            startMonthTwo = 10;
        }
        else if (startMonthTwo % 12 == 11) {
            startMonthTwo = 11;
        }
    }
    //based on month, pick the amount of days for the month
    //leap year included
    if ((startMonthTwo == 0) || (startMonthTwo == 2) || (startMonthTwo == 4) || (startMonthTwo == 6) || (startMonthTwo == 7) || (startMonthTwo == 9) || (startMonthTwo == 11)) {
        numDates = 31;
    }
    else if ((startMonthTwo == 3) || (startMonthTwo == 5) || (startMonthTwo == 8) || (startMonthTwo == 10)) {
        numDates = 30;
    }
    else if (startYearTwo % 4 == 0) {
        if (startYearTwo % 100 != 0) {
            numDates = 29;
        }
        else if (startYearTwo % 400 == 0) {
            numDates = 29;
        }
        else {
            numDates = 28;
        }
    }
    else {
        numDates = 28;
    }
    return numDates; //returns number of dates for month
}

//showMonth gets month of calendar and showing it to the
//main page as text
function showMonth(a) {
    console.log('show');
    if (a >= 0) {
        if (a % 12 == 0) { return "Jan"; }
        else if (a % 12 == 1) { return "Feb"; }
        else if (a % 12 == 2) { return "Mar"; }
        else if (a % 12 == 3) { return "Apr"; }
        else if (a % 12 == 4) { return "May"; }
        else if (a % 12 == 5) { return "Jun"; }
        else if (a % 12 == 6) { return "Jul"; }
        else if (a % 12 == 7) { return "Aug"; }
        else if (a % 12 == 8) { return "Sep"; }
        else if (a % 12 == 9) { return "Oct"; }
        else if (a % 12 == 10) { return "Nov"; }
        else if (a % 12 == 11) { return "Dec"; }
    }
    else {
        if (a % 12 == 0) { return "Jan"; }
        else if (Math.abs(a) % 12 == 1) { return "Dec"; }
        else if (Math.abs(a) % 12 == 2) { return "Nov"; }
        else if (Math.abs(a) % 12 == 3) { return "Oct"; }
        else if (Math.abs(a) % 12 == 4) { return "Sep"; }
        else if (Math.abs(a) % 12 == 5) { return "Aug"; }
        else if (Math.abs(a) % 12 == 6) { return "Jul"; }
        else if (Math.abs(a) % 12 == 7) { return "Jun"; }
        else if (Math.abs(a) % 12 == 8) { return "May"; }
        else if (Math.abs(a) % 12 == 9) { return "Apr"; }
        else if (Math.abs(a) % 12 == 10) { return "Mar"; }
        else if (Math.abs(a) % 12 == 11) { return "Feb"; }
    }
}

//addMonth changes the current month (and year if needed) forward by one
//after hitting next button.
function addMonth() {
    console.log('add');
    currentMonth = currentMonth + 1;
    document.getElementById("demo").innerHTML = showMonth(currentMonth);
    if (showMonth(currentMonth) == "Jan") {
        document.getElementById("demo2").innerHTML = currentYear;
    }
    clearCalendar();
    createCalendar();
}

//subMonth changes the current month (and year if needed) backward by one
//after hitting prev button.
function subMonth() {
    console.log('sub');
    currentMonth = currentMonth - 1;
    document.getElementById("demo").innerHTML = showMonth(currentMonth);
    if (showMonth(currentMonth) == "Dec") {
        document.getElementById("demo2").innerHTML = currentYear;
    }
    clearCalendar();
    createCalendar();
}

//showPlanner() displays the selected date to the planner side
//of the website
function showPlanner(tableCell) {
    console.log('show');

    selectedCell = tableCell;
    currentDate = tableCell.innerHTML;
    if (tableCell.innerHTML != " ") {
        document.getElementById("addApp").style.visibility = 'visible';
        d = new Date(currentYear, currentMonth, tableCell.innerHTML);
        d2 = d.toString();
        d3 = d2.substr(0, 10);
        document.getElementById("demo3").innerHTML = d3 + ', ' + d.getFullYear();
        document.getElementById("planner").style.display = "block";
        //document.getElementById("planner").style.display = "block";
    }
    else{
      document.getElementById("demo3").innerHTML = "";
      document.getElementById("addApp").style.visibility = 'hidden';
      document.getElementById("setAptStart").reset();
      document.getElementById("addDiv").style.visibility = 'hidden';
    }
    displayApt();
}


//getTime sets up getTimeMins to get meeting duration
function getTime() {
  console.log('getTime');
  var x = document.getElementById("mySelect").value;
  var y = document.getElementById("mySelect2").value;
  var startTime = x+y;
  var a = document.getElementById("mySelect3").value;
  var b = document.getElementById("mySelect4").value;
  var endTime = a+b;
  var dif = getTimeMins(x,y,a,b);
  return dif;
}

//getTimeMins gets the time duration for new meetings
function getTimeMins(x,y,a,b) {
    console.log('getTimeMins');
	var timeDif = 0;

   //date 1 and 2 are just meant to get time difference Not actual date.
    var date1 = new Date(2000, 0, 1,x,y);
	var date2 = new Date(2000, 0, 1, a,b);

    //if time goes over 24 hours
    if (date2 < date1) {
    	date2.setDate(date2.getDate() + 1);
	}

    timeDif = date2-date1;
    //convert into minutes
    var timeDifMins = timeDif/60000;
    //returns time difference in minutes
    return timeDifMins;
}

//addMeeting displays the add Meeting form to be filled out
function addMeeting(){

  var elem = document.getElementById('some_div');
  elem.style.maxHeight = "80px";
  document.getElementById("addDiv").style.visibility = 'visible';
}

//saveMeeting() activates after hitting confirm of add meeting form
//saves meeting info to new apt object. save apt object to toDatabase
//list. form resets and goes away while new apt objects are displayed to
//the screen
function saveMeeting() {
    var name1 = document.getElementById("fname").value;
    var name2 = document.getElementById("lname").value;
    if (!(isNaN(name1))||!(isNaN(name2))) {
      alert("Please enter patient's name");
    }
    else{

      b = confirm("Confirm Meeting?");
      if (b){
      year = currentYear;
      month = currentMonth;
      date = currentDate;
      meetLen = getTime();
      x = document.getElementById("mySelect").value;  //hour of start time
      y = document.getElementById("mySelect2").value;  //minutes of start time
      start = x+y;
      a = document.getElementById("mySelect3").value;
      b = document.getElementById("mySelect4").value;
      end = a+b;
      type = document.getElementById("mySelect5").value;
      dur = getTimeMins(x,y,a,b);
      //finalMeetStr holds the following info: year,month,date,startTime,len in minutes
      //this sting could be used to store in database
      finalMeetStr = year.toString() + month.toString() + date.toString()+ start + dur;

      meetList.push(finalMeetStr);

      //uncomment line below to see what's in list
      //document.getElementById("demo4").innerHTML = meetList;
      var meet = new apt(start.toString(),end.toString(),name1,name2,type,finalMeetStr,x,y,dur);

      var responseApt = post('/calendar/aptadd', {
        year: meet.year,
        month: meet.month,
        day: meet.date,
        firstName: meet.fn,
        lastName: meet.ln,
        hour: meet.hour,
        minute: meet.minute,
        duration: meet.duration,
        details: meet.type

      }, function(result) {
        var result = JSON.parse(result);
        document.getElementById("addDiv").style.visibility = 'hidden';
        console.log(result.success);
        displayApt("PUSH");
        if(result.success == false){
          alert(result.error);
        }
        else{
        document.getElementById("setAptStart").reset();
        }
      });

    }
  }
}

//definition of an appointment object. Each meeing has to save its
//start and end time, patient's first and last name, day of appointment,
//type of appointment, and a key to find meeting easier
function apt(st,et,fn,ln,type,key,hour,minute,duration){
  this.st = st; //start time
  this.et = et; //end time
  this.fn = fn; //first name
  this.ln = ln; //last name
  this.year = currentYear;   //year
  this.month = currentMonth + 1;  //month
  this.date = currentDate;
  //dateF holds the current date formated so that nums 0-9 have
  //a zero in front (00-09)
  if((currentDate >= 1) && (currentDate <= 9)){
    this.dateF = '0' + currentDate;
  }
  else{
    this.dateF = currentDate;
  }
  this.type = type; //detail of kind of meeting
  this.key = key;
  this.hour = hour;   //hour of start time
  this.minute = minute;  //hour of end time
  this.duration = duration; //duration of apt
}

//displayApt() makes sure only the apts for the selected date
//are displayed to the screen
function displayApt(purpose){
  get("/calendar/getapt", "year="+currentYear+"&month="+(currentMonth+1)+"&day="+currentDate, function(result) {
    var apts = JSON.parse(result);
    if (apts.length > 0){
      document.getElementById('some_div').innerHTML = '';
      for(var i = 0;i < apts.length;++i){
        string = " Start Time: " + apts[i].start.toString().slice(8,12) +
        "    End Time: " + apts[i].end.toString().slice(8,12) +
        apts[i].details;
        showPlannerMeetings(string,apts[i]);
      }
    }
    else{
      document.getElementById('some_div').innerHTML = '';
      document.getElementById("showMeet").innerHTML= "";
    }
  });
  var elem = document.getElementById('some_div');
  //console.log(elem);
  elem.style.maxHeight = "400px";
}

//cancel meeting clears the add meeting form and hids it
function cancelMeeting(){
  var elem = document.getElementById('some_div');
  elem.style.maxHeight = "400px";
  document.getElementById("setAptStart").reset();
  document.getElementById("addDiv").style.visibility = 'hidden';
}

//showPlannerMeetings() shows apt objects to the screen
function showPlannerMeetings(string,apt){
  // create the necessary elements
var label= document.createElement("label");
var place1= document.createElement("label");
var place2= document.createElement("label");
var description = document.createTextNode(string); //apt info
var btn1 = document.createElement("BUTTON");
var btn2 = document.createElement("BUTTON"); //delete button
var br1 = document.createElement("br");
var br2 = document.createElement("br");

//edit button info
/*
btn1.innerHTML = "Edit";
//btn1.onclick = function() {editMeeting(apt);}
document.body.appendChild(btn1);
label.appendChild(description);// add the description to the element
label.appendChild(br1); //break line
place1.appendChild(btn1);
*/

label.appendChild(description);// add the meeting info to the element
label.appendChild(br1); //break line

//delete button info
btn2.innerHTML = "Delete";
btn2.onclick = function() { deleteMeeting(apt); };
document.body.appendChild(btn2);
place2.appendChild(btn2);
place2.appendChild(br2);  //break line


// add the label element to your div
document.getElementById('some_div').appendChild(label);
document.getElementById('some_div').appendChild(place1);
document.getElementById('some_div').appendChild(place2);
//document.getElementById("btn2").onclick = deleteMeeting(apt);
}

//deleteMeeting() removes meeting from planner and toDatabase list
function deleteMeeting(apt){
  var response = confirm("Delete this meeting?");
  if(response){
  post('/calendar/delapt', {id:apt._id}, function(result) {
    displayApt("DELETE");
  });
}

}

//edit meeting will allow user to change apt info
function editMeeting(apt){
  currentApt = apt;

  var start = (apt.st).toString();
  var d1 = start.substr(0,2);
  var d2 = start.substr(2,2);

  var end = (apt.et).toString();
  var d3 = end.substr(0,2);
  var d4 = end.substr(2,2);

  document.getElementById("mySelect6").value = d1;
  document.getElementById("mySelect7").value = d2;
  document.getElementById("mySelect8").value = d3;
  document.getElementById("mySelect9").value = d4;

  document.getElementById("fname").value = apt.fn;
  document.getElementById("lname").value = apt.ln;
  document.getElementById("editDiv").style.visibility = 'visible';

}

//saveEditMeeting() saves changes to selected apt
function saveEditMeeting() {
  //currentApt
  for(i=0;i<toDatabase.length;++i){
    if (toDatabase[i].key == apt.key){
      index = i;
    }
  }

  x = document.getElementById("mySelect6").value;
  y = document.getElementById("mySelect7").value;
  start = x+y;
  a = document.getElementById("mySelect8").value;
  b = document.getElementById("mySelect9").value;
  end = a+b;
  type = document.getElementById("mySelect10").value;

  meetLen = getTimeMins(x,y,a,b);
  //finalMeetStr holds the following info: year,month,date,startTime,len in minutes
  //this sting could be used to store in database
  finalMeetStr = year.toString() + month.toString() + date.toString()+ start + meetLen;
  //toDatabase.push({finalMeetStr:[name1,name2]});
  //put new key into meeting list

  meetList[i]= finalMeetStr;
  //meetList.push(finalMeetStr);

  toDatabase[i].st = start;
  toDatabase[i].et = end;
  toDatabase[i].fn = fn;
  toDatabase[i].ln = ln;
  toDatabase[i].year = currentYear;
  toDatabase[i].month = currentMonth;
  toDatabase[i].date = currentDate;
  toDatabase[i].type = type;
  toDatabase[i].key = finalMeetStr;

  document.getElementById("addDiv").style.visibility = 'hidden';
  displayApt();
  document.getElementById("setAptStart").reset();
}
