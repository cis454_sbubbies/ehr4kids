var crypto = require('crypto');
const salt_len = 10;

var spacetime = require('spacetime');
const timeFormat = '{year}{iso-month}{date-pad}{hour-24-pad}{minute-pad}';

// database const
const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");
const ObjectId = require("mongodb").ObjectId;

// exported elements
var methods = {};


methods.url = "mongodb://npnguyen:TIbuZfiJslngcBpq2IZvQzmi8mKFRMKsBKHMI9PyKB4DkquMIE3kZbAhfD5sMQGQNPmk4NNxlgsH6pg9nJdQPQ%3D%3D@npnguyen.mongo.cosmos.azure.com:10255/?ssl=true&appName=@npnguyen@";

var client = new MongoClient(methods.url, { useUnifiedTopology: true });

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
var genRandomString = function(length){
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
};

/**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
var sha512 = function(password, salt){
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return value;
};

/**
 * login function: check username and password against database
 * @function
 * @param {string} username - username of account
 * @param {string} password - password of account
 * @return {bool} success - return whether login is successful
 */
methods.login = async function(username, password) {
    // connect to database
    var connected_client = await client.connect();
    console.log("Connected correctly to server for login");

    const db = connected_client.db("EHRKids");
    const col = db.collection("Users");

    // fetch users
    var cursor = col.find({ usr: username });
    var documents = await cursor.toArray();
    assert(documents.length <= 1, "database has duplicate usernamse!!");

    // compare password
    console.log("comparing pwd...");
    var result = false;
    if (documents.length == 1) {
        var account = documents[0];
        var { salt, hash } = account.pwd;
        var input_hash = sha512(password, salt);
        result = input_hash == hash;
    }
    return result;
};

/**
 * Check whether the numbers provided are valid dates in the calendar
 * @function
 * @param {number} year, month, day, hour, minute
 * @return {{ bool, String }}: { result, str }
 *          result - whether the input is valid
 *          str - if result is true, str contains the ISO format of the input
 */
var validate_date = function(year, month, day, hour, minute) {
    var date = new Date(Date.UTC(year, month - 1, day, hour, minute));
    if (isNaN(date)) return { result: false, str: null };
    return { result: date.getUTCFullYear() == year &&
        date.getUTCMonth() == month - 1 &&
        date.getUTCDate() == day &&
        date.getUTCHours() == hour,
        str: date.toISOString() };
};

/**
 * To date give number in the form of YYYYMMDDHHmm and get back date
 * @function
 * @params {number} number
 * @return validate_date result
 */
var to_date = function(number) {
    number = Math.floor(number);

    var minute = number % 100;
    number = Math.floor(number / 100);

    var hour = number % 100;
    number = Math.floor(number / 100);

    var day = number % 100;
    number = Math.floor(number / 100);

    var month = number % 100;
    number = Math.floor(number / 100);

    var year = number;
    return validate_date(year, month, day, hour, minute);
};

/**
 * Fetch schedule: fetch all events on a certain date for a certain user
 * @function
 * @param {string} usr - user whose schedule belongs to
 * @param {number} year
 * @param {number} month
 * @param {number} day
 * @return { [{object}] } documents - list of the events in the schedule
 *                                    return empty list if invalid date is provided
 */

methods.fetch_date_schedule = async function(usr, year, month, day) {
    var { result, str } = validate_date(year, month, day, 0, 0);
    if (!result) return [];

    var connected_client = await client.connect();
    console.log("Connected correctly to server");

    const db = connected_client.db("EHRKids");
    const col = db.collection("Schedule");

    var time = spacetime(str);
    var begin_day = Number(time.format(timeFormat));
    var end_day = Number(time.add(1, 'day').format(timeFormat));
    var cursor = col.find({
        usr: usr,
        $or: [
            { start: { $gte: begin_day, $lte: end_day } },
            { end: { $gt: begin_day, $lt: end_day} }
        ]
    });
    var array = await cursor.toArray();
    return array;
};

/**
 * Check if the event collides with existing ones
 * @function
 * @param {MongoClient} client - a connected mongodb client
 * @param {string} usr - the user whose events are checked
 * @param {number} start - event start time
 * @param {number} end - event end time
 * @param {ObjectId} ignore - id of an event you want to ignore
 * @return {bool} - whether the event can be inserted into the database or not
 */

var check_event_collision = async function(client, usr, start, end, ignore) {
    const db = client.db("EHRKids");
    const col = db.collection("Schedule");

    var cursor = col.find({
        _id: { $ne: ignore },
        usr: usr,
        $or: [
          { start: { $gte: start, $lt: end } },
          { end: {$gt: start, $lte: end} }
        ]
    });

    var cursor = cursor.limit(1);

    var documents = await cursor.toArray();
    return documents.length == 0;
};

/**
 * Push schedule: push a new event to the database
 * @function
 * @param {string} usr - user whose schedule belongs to
 * @param {number} year
 * @param {number} month
 * @param {number} day
 * @param {number} start - start time
 * @param {number} end - end time
 * @params {tbd} details - body of event
 * @return {Error {bool, string}} whether the operation is successful
 */

methods.push_schedule = async function(usr, year, month, day, hour, minute, duration, details) {
    // sanity check
    console.log({usr: usr, year: year, month: month, day: day, hour: hour, minute: minute, duration: duration, details: details});
    var { result, str } = validate_date(year, month, day, hour, minute);
    if (!result) return {
        success: false,
        error: 'invalid date'
    };

    if (duration <= 0) return {
        success: false,
        error: 'invalid duration'
    };

    var connected_client = await client.connect();
    console.log("Connected correctly to server");

    const db = client.db("EHRKids");
    const col = db.collection("Schedule");

    // check for event collisions
    var time = spacetime(str);

    var start = Number(time.format(timeFormat));
    var end = Number(time.add(duration, 'minute').format(timeFormat));

    var success = await check_event_collision(connected_client, usr, start, end);

    // push the event
    var result2 = {
        success: success,
    };

    if (result2.success) {
        var res = await col.insertOne({
            usr: usr,
            start: start,
            end: end,
            details: details
        });
        result2.id = res.insertedId;
    }
    else {
        result2.error = "event collision";
    }

    return result2;
};

/**
 * Delete event: delete an event from the calendar
 * @function
 * @param {ObjectId} id - the object id of the object
 *      *** ObjectId(someString) can be imported by require("mongodb").ObjectId
 * @return {{number - ok, number - count}} - if ok is 1, there is no error
 *                                           count is the number of documents the function deleted
 */

methods.delete_schedule = async function(id) {
    var connected_client = await client.connect();
    console.log("Connected correctly to server");

    const db = client.db("EHRKids");
    const col = db.collection("Schedule");

    var result = await col.deleteMany({ _id: id });
    return { ok: result.result.ok, count: result.result.n };
};

/**
 * Update schedule: update a new event to the database
 * @function
 * @param {string} usr - user whose schedule belongs to
 * @param {ObjectId} id - the object id of the object
 *      *** ObjectId(someString) can be imported by require("mongodb").ObjectId
 * @param {number} year
 * @param {number} month
 * @param {number} day
 * @param {number} start - start time
 * @param {number} end - end time
 * @params {tbd} details - body of event
 * @return {Error {bool, string}} whether the operation is successful
 */

methods.update_schedule = async function(id, usr, year, month, day, hour, minute, duration, details) {
    // sanity check
    var { result, str } = validate_date(year, month, day, hour, minute);
    if (!result) return {
        success: false,
        error: 'invalid date'
    };

    if (duration <= 0) return {
        success: false,
        error: 'invalid duration'
    };

    var connected_client = await client.connect();
    console.log("Connected correctly to server");

    const db = client.db("EHRKids");
    const col = db.collection("Schedule");

    // check for event collisions
    var time = spacetime(str);

    var start = Number(time.format(timeFormat));
    var end = Number(time.add(duration, 'minute').format(timeFormat));

    var success = await check_event_collision(connected_client, usr, start, end, id);

    // update the event
    var result2 = {
        success: success,
    };

    if (result2.success) {
        await col.updateOne(
            { _id: id },
            {
                $set: { start: start, end: end }
            }
        );
    }
    else {
        result2.error = "event collision";
    }

    return result2;
};


/**
 * Search detail schedule: return all events that has the input in its details
 * @function
 * @param {string} usr - user whose schedule belongs to
 * @param {string} detail - the input detail to search for
 * @return { [{object}] } documents - list of the events found
 */
methods.search_name_patient = async function(name) {
  var connected_client = await client.connect();
  console.log("Connected correctly to server");

  const db = connected_client.db("EHRKids");
  const col = db.collection("Patients");

  var cursor = col.find({
      "biography.patientName" : { $regex: name, $options: "im" }
  });
  var array = await cursor.toArray();
  return array;
};

methods.search_patient_id = async function(id) {
    var connected_client = await client.connect();
    console.log("Connected correctly to server");

    const db = connected_client.db("EHRKids");
    const col = db.collection("Patients");

    var cursor = col.find({
        _id: id
    });
    var array = await cursor.toArray();
    return array;
};

methods.push_patient = async function(patient) {
    var connected_client = await client.connect();
    console.log("Connected correctly to server");

    const db = connected_client.db("EHRKids");
    const col = db.collection("Patients");

    var res = await col.insertOne(patient);
    result.id = res.insertedId;

    return result;
};

methods.add_record_patient = async function(id, rid, details) {
    var connected_client = await client.connect();
    console.log("Connected correctly to server");

    const db = connected_client.db("EHRKids");
    const col = db.collection("Patients");
    var update = { $set : {}};
    update.$set["records."+rid] = details;
    var result = await col.updateOne(
        { _id: id },
        update
    );
    return result;
}

module.exports = methods;

//TEST FUNCTIONS

var test_validate_date = function() {
    var test_cases = [
        [-2021, 3, 4, 5, 6],
        [2019, 12, 24, 5, 39],
        [1234, 4, 3, 6, 7],
        [2020, 2, 29, 3, 4],
        // invalid inputs
        [2021, 2, 29, 4, 3],
        [2019, 13, 4, 3, 2],
        [2019, 0, 4, 3, 2],
        [2019, 3, 0, 3, 2],
        [2019, 3, 4, 24, 3]
    ];

    var results = test_cases.map(function(currentValue) {
        return validate_date(
            currentValue[0],
            currentValue[1],
            currentValue[2],
            currentValue[3],
            currentValue[4]);
    });

    console.log("Test validate date:");
    console.log(results);
};

var test_to_date = function() {
    var test_cases = [ 201912240539, 123404030607, 202002290304,
                       202102290403, 201913040302, 201903000302, 201903042403];

    var results = test_cases.map(function(currentValue) {
        return to_date(currentValue);
    });

    console.log("Test to date");
    console.log(results);
};

var test_push_schedule = function() {
    var push_schedule = methods.push_schedule;
    var delete_schedule = methods.delete_schedule;

    var invalid_date = push_schedule("dummy", 2019, 1, 0, 0, 0, 10, "details");
    var invalid_duration = push_schedule("dummy", 2019, 1, 2, 0, 0, 0, "details");

    var stc = async function() { // success then collision
        var first_push = await push_schedule("dummy", 2019, 1, 1, 0, 0, 10, "details");
        var second_push = await push_schedule("dummy", 2019, 1, 1, 0, 20, 10, "details");
        var third_push = await push_schedule("dummy", 2019, 1, 1, 0, 10, 10, "details");
        var fourth_push = await push_schedule("dummy", 2019, 1, 1, 0, 10, 10, "details");

        console.log("CLEANING UP");

        console.log(await delete_schedule(first_push.id));
        console.log(await delete_schedule(second_push.id));
        console.log(await delete_schedule(third_push.id));

        return [first_push, second_push, third_push, fourth_push];
    }();
    var values = Promise.all([invalid_date, invalid_duration, stc]).then(function(values) {
        console.log("Test push schedule");
        console.log(values);
        test_update_schedule();
        return values;
    });
};

var test_update_schedule = function() {
    var update_schedule = methods.update_schedule;
    var id = ObjectId("5e814ac8ed936f59ca1da230");

    var invalid_date = update_schedule(id, "dummy", 2018, 1, 0, 0, 0, 10, "details");
    var invalid_duration = update_schedule(id, "dummy", 2018, 1, 2, 0, 0, 0, "details");
    var collision = update_schedule(id, "dummy", 2018, 1, 1, 0, 10, 20, "details");
    var success = async function() {
        var update = await update_schedule(id, "dummy", 2018, 1, 1, 0, 10, 5, "details");
        var cleanup = await update_schedule(id, "dummy", 2018, 1, 1, 0, 10, 10, "details");
        return [update, cleanup];
    }();

    var values = Promise.all([invalid_date, invalid_duration, collision, success]).then(function(values) {
        console.log("Test update schedule");
        console.log(values);
        test_fetch_date();
        return values;
    });
};

var test_fetch_date = function() {
    var fetch_date = methods.fetch_date_schedule;

    var result = fetch_date("dummy", 2018, 1, 1);
    result.then(function(value) {
        console.log("Test fetch schedule");
        console.log(value);
        test_search_detail();
        return value;
    });
};

var test_search_detail = function() {
    var search_detail = methods.search_detail_schedule;
    var result1 = search_detail("dummy", "dummy");
    var result2 = search_detail("dummy", "lolk");
    var result3 = search_detail("dummy", "details");

    Promise.all([result1, result2, result3]).then(function(values) {
        console.log("Test search details");
        console.log(values);
    });
};

//TEST CASES

//test_validate_date();
//test_to_date();
//test_push_schedule();
