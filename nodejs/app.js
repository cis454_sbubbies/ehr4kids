var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var helmet = require('helmet')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var loginRouter = require('./routes/login');
var logoutRouter = require('./routes/logout');
var calendarRouter = require('./routes/calendar');
var recordRouter = require('./routes/records');
var enrollRouter = require('./routes/enroll');
var app = express();

//Create HTTPS Server
var https = require('https');
var fs = require('fs');
var keyOptions = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};
var serverHTTPS = https.createServer(keyOptions, app).listen(443, function(){
  console.log("Express server listening on port 443");
});

var mg_url = require('./backend').url;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


var sess = {
  secret: 'keyboard schrodinger cat',
  name: 'sessID',
  resave: false,
  rolling: true,
  saveUninitialized: false,
  store: new MongoStore({
      url: mg_url,
      touchAfter: 24 * 3600, // time period in seconds
      collection: 'sessions',
      autoRemove: 'interval',
      autoRemoveInterval: 10 // In minutes. Default
  }),
  cookie: { path: '/', httpOnly: true, maxAge: 1000 * 60 * 30 }
};

function checkValidUser(req, res, next) {
  if (req.session.uname || 0) {
    req.session.cookie.maxAge = 1000 * 60 * 30;
    next();
  } else {
    res.redirect('/login');
  }
}

app.use(function(req, res, next) {
  if(!req.secure) {
    res.redirect("https://" + req.headers.host + req.url);
  }
  next();
});

if (app.get('env') === 'production') {
  app.set('trust proxy', 1); // trust first proxy
  sess.cookie.secure = true; // serve secure cookies
}

app.use(helmet());
app.use(session(sess));

app.use('/', loginRouter);
app.use('/login', loginRouter);
app.use('/logout', checkValidUser, logoutRouter);
app.use('/users', checkValidUser, usersRouter);
app.use('/calendar', checkValidUser, calendarRouter);
app.use('/records', checkValidUser, recordRouter);
app.use('/enroll', checkValidUser, enrollRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(res.locals.message);
  console.log(res.locals.error);
  // render the error page
  res.status(err.status || 500);
  res.render('error', {
    status: err.status
  });
});

module.exports = app;
