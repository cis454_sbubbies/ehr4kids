$(function(){

	var cache = {'':$('.shower-deeper')};

	$(window).bind( 'hashchange', function(e) {
		var url = $.param.fragment();
		console.log(url);

		$( 'a.bbq-current' ).removeClass( 'bbq-current' );

		$('.shower-deep').children(':visible').hide();

		url && $( 'a[href="#' + url + '"]' ).addClass( 'bbq-current' );
		if(cache[url]){
			cache[url].show();
		}else{
			cache[url]=$('<div class="toBeAdded" style="background:yellow;">').appendTo('.shower-deep').load(String('./htmls/'+url));
		}

	});

	$(window).trigger('hashchange');

});
