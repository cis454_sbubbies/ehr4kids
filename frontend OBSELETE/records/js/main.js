//get record and display to screen
function getRecord(elmnt) {
  try{
    var recordId = String(localStorage.getItem('lastRecordViewed')+'1');
    if(recordId.length!=0){
      console.log('Hurray');
      console.log('Last Record Was: '+recordId);
      var lastElement = document.getElementById(recordId);
      lastElement.style.backgroundColor = "white";
    }
  }catch(error){
    console.log('Boo');
  }
  $(document).ready(function(){
    // change color to active color
    elmnt.style.backgroundColor = "#FFBF00";

    var patient = JSON.parse(localStorage.getItem('patientData'));
    var currentPatientRecord = (patient.records[String($("input[name='recordWidget']:checked").val())]);
    console.log(currentPatientRecord.recordTitle);

    $("#mainRightBodyRight").html('<div class="displayFrame"><h2>'+currentPatientRecord.recordTitle+'</h2><br><h4>Patient Name: '+patient.biography.patientName+'</h4><h4>Patient ID: '+patient.patientId+'</h4><h4>Hospital: '+currentPatientRecord.hospital+'</h4><h4>Doctor: '+currentPatientRecord.doctor+'</h4><h4>Date: '+currentPatientRecord.date+'  Time:'+currentPatientRecord.time+'</h4><br><p>'+currentPatientRecord.recordContent+'</p></div>');

    localStorage.setItem('lastRecordViewed',$("input[name='recordWidget']:checked").val());
  });
}

//generate the list of records on the left hand
//also generates pateint record mgr button
function generateRecordList(){
  var patient = JSON.parse(localStorage.getItem('patientData'));
  // if list is empty or is a different patient
  $('#recordList').empty();

  $('.mainRightBody').html('<div class="mainRightBodyLeft"><form id = "recordList"><!--Records will be generated and put here by JSON--></form></div><div id="mainRightBodyRight"></div>');
  // try to learn to link back to css document
  $('.mainRightBody').css({"width":"100%","height":"calc(100% - 14.4em)","display":"grid","grid-template-columns":"430px auto"});

  $.each(patient.records, function(key, value){
  console.log(key+': '+this.hospital);
  $('#recordList').append('<input type="radio" name="recordWidget" id="'+key+'" value="'+key+'" hidden><label for = "'+key+'" class="recordLabel"><div class = "record" id="'+key+'1"onclick="getRecord(this)"> <div class="recordContainer"><div class="recordInfo"> <h4>'+ patient.biography.patientName+'</h4> <h6>'+this.hospital+'</h6><p>'+this.doctor+'<br>'+this.date+':'+this.time+'</p></div><div class="recordType"><h4>'+this.recordType+'</h4></div></div></div></label>');
  });
//Generate Patient Button in right corner
  $(".mainRightTopToolBarBoxRightContainer").html('<input type="checkbox" id="patientWindowCheck" onclick="recordMgCheck(this)" hidden><label for="patientWindowCheck" class="patientInfoBoxContainer"><div class="patientInfoBox"><h4>'+
  patient.biography.patientName+'</h4><h6>Address: '+patient.biography.address+'</h6><p>Gender: '+patient.biography.sex +'<br>Dob: '+patient.biography.DOB +'</p></div></label>');
}

// calls patient data jsonfile from server
function getPatientData(element){
  var patientId = document.getElementById("patientIdSearch").value;
  fetch("./dummyFetchRecords/"+ patientId.trim() + ".json")
    .then(function(resp){
      return resp.json();
    })
    .then(function(data){
      document.getElementById("patientNotFound").style.display = "none";
      localStorage.setItem('patientData',JSON.stringify(data));
      console.log(data);
      generateRecordList();
      //add a rule to reload div and uncheck button
    }).catch(function(error){
      document.getElementById("patientNotFound").style.display = "inline";

      console.log(error);
    });
}

// if enter pushed on search Quality of Life(QOL)
var input = document.getElementById("patientIdSearch");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById("patientIdSearchButton").click();
  }
});

// handling filter checkboxes
function filterRecords(){
  // this functions will scale poorly in future it should be optimized currently running at O(NxM) but it is fine for testing purposes
  var listOfChecked = [];
  $('.filtersContainer').each(function(){
    $(this).find('.checkBoxContainer').each(function(){
      $(this).find('.filterCheckBox').each(function(){
        if($(this).prop("checked")){
          listOfChecked.push($(this).val());
        }});});});
  console.log(listOfChecked);
  if(listOfChecked.length==0){
    $('#recordList').each(function(){
      $(this).find('label').each(function(){
        $(this).show();
      });
    });
  }else{
    $('#recordList').each(function(){
      $(this).find('label').each(function(){
        if(!(listOfChecked.includes(($(this).find(".recordType").text())))){
          $(this).hide();
        }else{
          $(this).show();
        }
      });
    });
  }

}

// reload body dive to mannage records page
function recordMgCheck(recordBox){
  if(recordBox.checked) {
     localStorage.setItem('mainRightBodyLastState',$(".mainRightBody").html());
     $('.mainRightBody').html('<object type="text/html" data="patientPage.html" class="patientRecordPageDisplay"></object>');
     $('.mainRightBody').css("display","block");
   } else {
     $(".mainRightBody").html(localStorage.getItem('mainRightBodyLastState'));
     $(".mainRightBody").css({"display":"grid","grid-template-columns":"430px auto"}) ;
   }
}
