var inputs = $('.recordInputActive');
var form = {};
var patient = JSON.parse(localStorage.getItem('patientData'));
var recordId = Math.round(Math.random() * 10);
var popUp = $('#previewPopup');

function stageRecord(){
	inputs.each(function(){
		$(this).attr('disabled',true);
		$(this).removeClass('recordInputActive');
		$(this).addClass('recordInputStaged');
	});
	$("#addRecordButton").attr('hidden',true);
	$("#eidtRecordButton").attr('hidden',false);

	$("#confirmRecordButton").attr('hidden',false);
	$('#previewPopup').show();

}

function closePopUp(){
	$('#previewPopup').hide();
}

function unStageRecord(){
	inputs.each(function(){
		$(this).attr('disabled',false);
		$(this).removeClass('recordInputStaged');
		$(this).addClass('recordInputActive');
	});
	$("#addRecordButton").attr('hidden',false);
	$("#eidtRecordButton").attr('hidden',true);
	$("#confirmRecordButton").attr('hidden',true);
}

function commitRecord(){
	inputs.each(function(){
		form[String($(this).attr('id'))]=$(this).val();
	});
	console.log(form);
	if(!(patient.records[String(recordId+'-'+patient.patientId)])){
		patient.records[String(recordId+'-'+patient.patientId)]=form;
		console.log(patient);
		$('#formContainer').attr('hidden',true);
		$('#pushSucessful').css('display','grid');
	}
}
