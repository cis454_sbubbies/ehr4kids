
var forms = $('#welcome');
$('.mainRightBodyContentPadding').find('.enrollmentForm').each(function(){
	forms.push($(this));
});
forms.push($('#confirmEnrollment'));
var percent = 0;
var state=0;

percent = (1/forms.length)*100;
$('#progressBar').css('width',String(percent+'%'));

function navigateEnrollment(string){

	if(string=='previous'){
		$(forms).each(function(){
			$(this).hide();
		});
		state--;
		$(forms[state]).show();
		percent = ((state+1)/forms.length)*100;
		$('#progressBar').css('width',String(percent+'%'));
	}else{
		$(forms).each(function(){
			$(this).hide();
		});
		state++;
		$(forms[state]).show();
		percent = ((state+1)/forms.length)*100;
		$('#progressBar').css('width',String(percent+'%'));
	}
	if(state!=0&&state!=(forms.length-1)){$('#formNavButtons').show()}
	else{$('#formNavButtons').hide()}

	if(state==(forms.length-1)){
		$(forms[1]).show();
		$("#pleaseReviewform").show();
		$(forms[1]).children().prop( "disabled", true );
	}else{
		$("#pleaseReviewform").hide();
		$(forms[1]).children().prop( "disabled", false );
	}
}

console.log(forms.length);
forms.each(function(){
	console.log($(this).attr('id'));
});

function setHeight() {
    $(".mainRightBodyContainer").height($(".mainRight").height() - ($(".mainRightTopHeader").height()*2));
}

function createPatient(){
	var patientObject =
	'{' +
		'"biography": { '+
			'"patientName": "'+ $('#studentName').val() +'",' +
			'"class": "'+$('#classYear').val()+'",' +
			'"address": "'+$('#address').val()+'",' +
			'"sex": "'+ $('#sex').val()+'",' +
			'"DOB": "'+$('#DOB').val() +'" '+
		'},' +
		'"parents": {' +
			'"father": {' +
				'"name": "'+ $('#fatherName').val()+'",' +
				'"occupation": "'+ $('#fatherOccupation').val()+'"' +
			'},' +
			'"mother": {' +
				'"name": "'+$('#motherName').val() +'",' +
				'"occupation": "'+$('#motherOccupation').val() +'"' +
			'}' +
		'},' +
		'"biometrics": {},'  +
		'"records": {}}';
	alert(JSON.stringify(JSON.parse(patientObject)));
	//router.post("/records", JSON.parse(patientObject), method = 'post');

}

$(document).ready(setHeight);
$(window).resize(setHeight);
